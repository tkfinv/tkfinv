﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TKFINV.Web
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class tkfinvEntities : DbContext
    {
        public tkfinvEntities()
            : base("name=tkfinvEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<applicationmodule> applicationmodule { get; set; }
        public DbSet<applicationmodulegroup> applicationmodulegroup { get; set; }
        public DbSet<applicationuser> applicationuser { get; set; }
        public DbSet<applicationuserrole> applicationuserrole { get; set; }
        public DbSet<appuserrole_module> appuserrole_module { get; set; }
        public DbSet<factory> factory { get; set; }
        public DbSet<inventory> inventory { get; set; }
        public DbSet<inventorycurrently> inventorycurrently { get; set; }
        public DbSet<inventorydailymovement> inventorydailymovement { get; set; }
        public DbSet<inventorytype> inventorytype { get; set; }
        public DbSet<location> location { get; set; }
        public DbSet<packageunit> packageunit { get; set; }
        public DbSet<productmodel> productmodel { get; set; }
        public DbSet<productsize> productsize { get; set; }
        public DbSet<warehouse> warehouse { get; set; }
    }
}
