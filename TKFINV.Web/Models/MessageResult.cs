﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TKFINV.Web.Models
{
    public class MessageResult
    {
        public MessageStatus status { get; set; }
        public string message { get; set; }
    }
}