﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TKFINV.Web.Models
{
    public class applicationuserroleModel : IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "กรุณากรอกข้อมูลชื่อ Role")]
        public string roleName { get; set; }

        public string description { get; set; }
        public bool isActive { get; set; }
        public string createBy { get; set; }
        public Nullable<System.DateTime> createDate { get; set; }
        public string updateBy { get; set; }
        public Nullable<System.DateTime> updateDate { get; set; }

        public int UserRoleCount { get; set; }

        public List<applicationmodule> moduleList { get; set; }
        //public List<company> companyList { get; set; }
        //public List<branch> branchList { get; set; }
        public List<int> branch_select { get; set; }
        public List<int> appmodule_select { get; set; }
    }
}