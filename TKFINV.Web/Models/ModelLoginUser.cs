﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TKFINV.Web.Models
{
    public class LoginUserModel
    {
        public int ID { get; set; }
        public int ApplicationUserRoleID { get; set; }
        public string username { get; set; }
        public string password { get; set; }
       
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string avatarFile { get; set; }
        public bool isActive { get; set; }
        public string loginNotifymessage { get; set; }
        public DateTime? lastLogin { get; set; }

        public List<applicationmodule> appmodulelist { get; set; }
        public List<applicationmodulegroup> appmodulegrouplist { get; set; }

    }
}