﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TKFINV.Web.Models
{
    public class applicationuserModel : IModel
    {
        public int ID { get; set; }
        public int ApplicationUserRoleID { get; set; }

        [Required(ErrorMessage = "กรุณากรอกข้อมูลชื่อผู้ใช้งาน")]
        [RegularExpression("^[a-zA-Z0-9_]{5,20}$", ErrorMessage = "กรุณาระบุชื่อผู้ใช้งานเป็นตัวเลข หรือตัวอักษรความยาวระหว่าง 5-20 ตัวอักษร")]
        public string username { get; set; }

        //[Required(ErrorMessage = "กรุณากรอกรหัสผ่าน")]
        [RegularExpression(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[.!@#$%^&+=/\\]).*$", ErrorMessage = "ความยาวไม่น้อยกว่า 8 ตัวอักษร ประกอบด้วยตัวอักษรภาษาอังกฤษตัวเล็ก ตัวใหญ่ ตัวเลข และอักขระพิเศษต่อไปนี้ [. ! @ # $ % ^ & + = / \\]อย่างน้อย 1 ตัว")]
        public string password { get; set; }

        [Compare("password", ErrorMessage = "ยืนยันรหัสผ่านไม่ตรงกับรหัสผ่านใหม่")]
        public string confirmPassword { get; set; }

        public Nullable<System.DateTime> lastLogin { get; set; }

        [Required(ErrorMessage = "กรุณากรอกชื่อ")]
        public string firstName { get; set; }

        public string lastName { get; set; }

        [Required(ErrorMessage = "กรุณากรอกอีเมล")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "กรุณาระบุอีเมลล์ในรูปแบบที่ถูกต้อง")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string fax { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public Nullable<int> zipcode { get; set; }
        public string country { get; set; }
        public string avatarFile { get; set; }
        public bool isActive { get; set; }
        public string createBy { get; set; }
        public Nullable<System.DateTime> createDate { get; set; }
        public string updateBy { get; set; }
        public Nullable<System.DateTime> updateDate { get; set; }

        public virtual applicationuserrole applicationuserrole { get; set; }
    }
}