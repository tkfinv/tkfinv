﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TKFINV.Web.Models
{
    public enum MessageStatus
    {
        error,
        warning,
        success,
        info
    }

    public class MessageText
    {
        public string update { get { return "บันทึกการแก้ไขเรียบร้อย"; } }
        public string insert { get { return "บันทึกข้อมูลเรียบร้อย"; } }
        public string delete { get { return "ลบข้อมูลเรียบร้อย"; } }
        public string insert_duplicate { get { return "ไม่สามารถ"; } }

    }
}