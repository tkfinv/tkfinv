﻿using TKFINV.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKFINV.Web.Controllers
{
    public class HomeController : _BaseController
    {
        public ActionResult Index()
        {
            var user = (LoginUserModel)Session["Appuser"];
            if (user == null)
                user = new LoginUserModel();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
