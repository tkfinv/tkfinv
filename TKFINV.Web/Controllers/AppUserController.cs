﻿using TKFINV.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using TKFINV.Web.Models;

namespace TKFINV.Web.Controllers
{
    public class AppUserController : _BaseController
    {
        //
        // GET: /AppUser/

        private List<SelectListItem> RoleItem(string roleid)
        {
            List<SelectListItem> item_list = new List<SelectListItem>();
            var data = new AppRoleBLL().getRole();
            if (data != null && data.Count() > 0)
            {
                item_list.Add(new SelectListItem() { Value = "", Text = "Select All" });
                foreach (var item in data)
                {
                    item_list.Add(new SelectListItem() { Value = item.ID.ToString(), Text = item.description, Selected = (roleid == item.ID.ToString() ? true : false) });
                }
            }

            return item_list;


        }

        private List<SelectListItem> RoleItem()
        {
            List<SelectListItem> item_list = new List<SelectListItem>();
            var data = new AppRoleBLL().getRole();
            if (data != null && data.Count() > 0)
            {
                foreach (var item in data)
                {
                    item_list.Add(new SelectListItem() { Value = item.ID.ToString(), Text = item.description });
                }
            }

            return item_list;


        }

        private List<SelectListItem> ActiveItem()
        {
            List<SelectListItem> item_list = new List<SelectListItem>();
            item_list.Add(new SelectListItem() { Value = "true", Text = "Active" });
            item_list.Add(new SelectListItem() { Value = "false", Text = "In Active" });

            return item_list;


        }

        private List<SelectListItem> AvatarItem()
        {
            List<SelectListItem> item_list = new List<SelectListItem>();
            item_list.Add(new SelectListItem() { Value = "avatar.png", Text = " " });
            item_list.Add(new SelectListItem() { Value = "avatar1.jpg", Text = "avatar1" });
            item_list.Add(new SelectListItem() { Value = "avatar2.jpg", Text = "avatar2" });
            item_list.Add(new SelectListItem() { Value = "avatar3.jpg", Text = "avatar3" });
            item_list.Add(new SelectListItem() { Value = "avatar4.jpg", Text = "avatar4" });
            item_list.Add(new SelectListItem() { Value = "avatar5.jpg", Text = "avatar5" });

            return item_list;


        }

        int pageSize = 20;

        private ActionResult DataBind(int? page, string keyword, string keyrole, string orderby, string orderDESC)
        {
            bool desc = orderDESC == null ? false : orderDESC == "" ? false : orderDESC == "false" ? false : true;
            int pageNumber = (page ?? 1);

            ViewBag.keyrole = keyrole;
            ViewBag.keyword = keyword;
            ViewBag.orderby = orderby == null ? "username" : orderby == "" ? "username" : orderby;
            ViewBag.orderDESC = desc.ToString().ToLower();
            ViewBag.page = page;
            ViewBag.StartRowNumber = (pageNumber * pageSize) - pageSize;

            IEnumerable<applicationuser> listObject = new AppUserBLL().getList(keyword, keyrole, orderby, desc);
            ViewBag.TotalRecords = listObject.Count();

            var listPaged = listObject.ToPagedList(pageNumber, pageSize);
            //var listPaged = listObject;
            if (listPaged == null)
                return HttpNotFound();

            ViewBag.ListData = listPaged;
            ViewBag.RoleItem = this.RoleItem(keyrole);
            ViewBag.MessageAlert = TempData["MessageAlert"];
            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_List") : View();

        }

        public ActionResult Index(int? page, string keyword, string keyrole, string orderby, string orderDESC, string dummy)
        {
            return DataBind(page, keyword, keyrole, orderby, orderDESC);

        }

        public ActionResult Detail(string rowid, int? page, string keyword, string keyrole, string orderby, string orderDESC)
        {
            bool desc = orderDESC == null ? false : orderDESC == "" ? false : orderDESC == "false" ? false : true;
            int pageNumber = (page ?? 1);
            int row_id = 0;
            try
            {
                var decode = UtilsBLL.Decrypt_QueryString(rowid);
                row_id = int.Parse(decode);
            }
            catch
            {
                row_id = 0;
            }


            ViewBag.keyrole = keyrole;
            ViewBag.keyword = keyword;
            ViewBag.orderby = orderby == null ? "username" : orderby == "" ? "username" : orderby;
            ViewBag.orderDESC = desc.ToString().ToLower();
            ViewBag.page = page;
            ViewBag.rowid = rowid;
            ViewBag.MessageAlert = (MessageResult)TempData["MessageAlert"];

            var app_user = new AppUserBLL().getModelObjectByID(row_id);
            //initial data
            if (row_id == 0)
            {
                app_user.isActive = true;
            }

            ViewBag.RoleItem = this.RoleItem();
            ViewBag.ActiveItem = this.ActiveItem();
            ViewBag.AvatarItem = this.AvatarItem();
            return View(app_user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Detail(int? page, string keyword, string keyrole, string orderby, string orderDESC, string rowid, applicationuserModel model)
        {
            bool desc = orderDESC == null ? false : orderDESC == "" ? false : orderDESC == "false" ? false : true;
            int pageNumber = (page ?? 1);
            var curr_user = (TKFINV.Web.Models.LoginUserModel)Session["Appuser"];
            int row_id = 0;
            try
            {
                var decode = UtilsBLL.Decrypt_QueryString(rowid);
                row_id = int.Parse(decode);
            }
            catch
            {
                row_id = 0;
            }

            ViewBag.keyrole = keyrole;
            ViewBag.keyword = keyword;
            ViewBag.orderby = orderby == null ? "username" : orderby == "" ? "username" : orderby;
            ViewBag.orderDESC = desc.ToString().ToLower();
            ViewBag.page = page;
            ViewBag.rowid = rowid;
            model.ID = row_id;


            if (row_id > 0)
            {
                model.updateBy = curr_user.username;

                var result = new AppUserBLL().Update(model);
                ViewBag.MessageAlert = result.MessageResult;
                TempData["MessageAlert"] = result.MessageResult;
                if (result.MessageResult != null && result.MessageResult.status == MessageStatus.success)
                    return RedirectToAction("Detail", new { rowid = UtilsBLL.Encrypt_QueryString(model.ID.ToString()), page = page, keyword = keyword, keyrole = keyrole, orderby = orderby, orderDESC = orderDESC });
            }
            else
            {
                model.createBy = curr_user.username;

                var result = new AppUserBLL().Insert(model);
                model.ID = result.ID;
                ViewBag.MessageAlert = result.MessageResult;
                TempData["MessageAlert"] = result.MessageResult;
                if (result.MessageResult != null && result.MessageResult.status == MessageStatus.success)
                    return RedirectToAction("Detail", new { rowid = UtilsBLL.Encrypt_QueryString(model.ID.ToString()), page = page, keyword = keyword, keyrole = keyrole, orderby = orderby, orderDESC = orderDESC });
            }


            ViewBag.RoleItem = this.RoleItem();
            ViewBag.ActiveItem = this.ActiveItem();
            ViewBag.AvatarItem = this.AvatarItem();
            return View(model);
        }

        public PartialViewResult ConfirmDelete(int? page, string keyword, string keyrole, string orderby, string orderDESC, string rowid, string description)
        {

            ViewBag.keyrole = keyrole;
            ViewBag.rowid = rowid;
            ViewBag.description = description;
            bool desc = orderDESC == null ? false : orderDESC == "false" ? false : true;
            ViewBag.keyword = keyword;
            ViewBag.orderby = orderby == null ? "ID" : orderby == "" ? "ID" : orderby;
            ViewBag.orderDESC = desc.ToString().ToLower();
            ViewBag.page = page;
            return PartialView("ConfirmDelete");
        }

        public ActionResult DeleteData(int? page, string keyword, string keyrole, string orderby, string orderDESC, string rowid)
        {
            int row_id = 0;
            try
            {
                var decode = UtilsBLL.Decrypt_QueryString(rowid);
                row_id = int.Parse(decode);
            }
            catch
            {
                row_id = 0;
            }

            var result = new AppUserBLL().Delete(row_id);
            TempData["MessageAlert"] = result.MessageResult;

            return DataBind(page, keyword, keyrole, orderby, orderDESC);
        }

        public ActionResult PartialEmpty()
        {
            return new EmptyResult();
        }

    }
}
