﻿using TKFINV.Web.BusinessLogic;
using TKFINV.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKFINV.Web.Controllers
{
    public class ProfileController : Controller
    {
        //
        // GET: /Profile/

        private List<SelectListItem> AvatarItem()
        {
            List<SelectListItem> item_list = new List<SelectListItem>();
            item_list.Add(new SelectListItem() { Value = "avatar.png", Text = " " });
            item_list.Add(new SelectListItem() { Value = "avatar1.jpg", Text = "avatar1" });
            item_list.Add(new SelectListItem() { Value = "avatar2.jpg", Text = "avatar2" });
            item_list.Add(new SelectListItem() { Value = "avatar3.jpg", Text = "avatar3" });
            item_list.Add(new SelectListItem() { Value = "avatar4.jpg", Text = "avatar4" });
            item_list.Add(new SelectListItem() { Value = "avatar5.jpg", Text = "avatar5" });

            return item_list;


        }

        public ActionResult Index()
        {
            if (Session["Appuser"] == null)
                return RedirectToAction("index", "Login");

            var login_user = (LoginUserModel)Session["Appuser"];
            var appuser = new AppUserBLL().getModelObjectByID(login_user.ID);

            ViewBag.AvatarItem = this.AvatarItem();
            return View(appuser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(applicationuserModel model)
        {
            if (Session["Appuser"] == null)
                return RedirectToAction("index", "Login");

            var login_user = (LoginUserModel)Session["Appuser"];
            model.ID = login_user.ID;
            model.isActive = true;
            model.updateBy = login_user.username;
            model = new AppUserBLL().Update(model);
            ViewBag.MessageAlert = model.MessageResult;
            ViewBag.AvatarItem = this.AvatarItem();
            return View(model);
        }

    }
}
