﻿using TKFINV.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using TKFINV.Web.Models;

namespace TKFINV.Web.Controllers
{
    public class AppRoleController : _BaseController
    {
        //
        // GET: /AppRole/

        private List<SelectListItem> ActiveItem()
        {
            List<SelectListItem> item_list = new List<SelectListItem>();
            item_list.Add(new SelectListItem() { Value = "true", Text = "Active" });
            item_list.Add(new SelectListItem() { Value = "false", Text = "In Active" });
            
            return item_list;

        }

        int pageSize = 20;

        private ActionResult DataBind(int? page, string keyword, string orderby, string orderDESC)
        {
            bool desc = orderDESC == null ? false : orderDESC == "" ? false : orderDESC == "false" ? false : true;
            int pageNumber = (page ?? 1);
            orderby = (string.IsNullOrEmpty(orderby) ? "roleName" : orderby);

            ViewBag.keyword = keyword;
            ViewBag.orderby = orderby;
            ViewBag.orderDESC = desc.ToString().ToLower();
            ViewBag.page = page;
            ViewBag.StartRowNumber = (pageNumber * pageSize) - pageSize;

            var listObject = new AppRoleBLL().getList(keyword, orderby, desc);
            ViewBag.TotalRecords = listObject.Count();

            var listPaged = listObject.ToPagedList(pageNumber, pageSize);
            //var listPaged = listObject;
            if (listPaged == null)
                return HttpNotFound();

            ViewBag.ListData = listPaged;
            ViewBag.message_result = TempData["message_result"];
            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_List") : View();

        }

        public ActionResult Index(int? page, string keyword, string orderby, string orderDESC)
        {
            return DataBind(page, keyword, orderby, orderDESC);
        }

        public ActionResult Detail(string rowid, int? page, string keyword, string orderby, string orderDESC)
        {
            bool desc = orderDESC == null ? false : orderDESC == "" ? false : orderDESC == "false" ? false : true;
            orderby = (string.IsNullOrEmpty(orderby) ? "roleName" : orderby);
            int pageNumber = (page ?? 1);
            int row_id = 0;
            try
            {
                var decode = UtilsBLL.Decrypt_QueryString(rowid);
                row_id = int.Parse(decode);
            }
            catch
            {
                row_id = 0;
            }

            ViewBag.keyword = keyword;
            ViewBag.orderby = orderby;
            ViewBag.orderDESC = desc.ToString().ToLower();
            ViewBag.page = page;
            ViewBag.rowid = rowid;
            ViewBag.message_result = (MessageResult)TempData["message_result"];

            var app_data = new AppRoleBLL().getByID(row_id);
            //initial data
            if (row_id == 0)
            {
                app_data.isActive = true;
               // app_data.branchList = new List<branch>();
               // app_data.companyList = new List<company>();
                app_data.moduleList = new List<applicationmodule>();
            }

            
            ViewBag.ActiveItem = this.ActiveItem();
            ViewBag.ModuleGroupAll = new AppRoleBLL().getAllModuleGroup();
            ViewBag.ModuleAll = new AppRoleBLL().getAllModule();
            //ViewBag.CompamyAll = new AppRoleBLL().getAllCompany();
            //ViewBag.BranchAll = new AppRoleBLL().getAllBranch();
            return View(app_data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Detail(int? page, string keyword, string orderby, string orderDESC, string rowid, applicationuserroleModel model, int[] appmodule, int[] branchapp)
        {
            bool desc = orderDESC == null ? false : orderDESC == "" ? false : orderDESC == "false" ? false : true;
            orderby = (string.IsNullOrEmpty(orderby) ? "roleName" : orderby);
            int pageNumber = (page ?? 1);
            var curr_user = (TKFINV.Web.Models.LoginUserModel)Session["Appuser"];
            int row_id = 0;
            try
            {
                var decode = UtilsBLL.Decrypt_QueryString(rowid);
                row_id = int.Parse(decode);
            }
            catch
            {
                row_id = 0;
            }

            ViewBag.keyword = keyword;
            ViewBag.orderby = orderby;
            ViewBag.orderDESC = desc.ToString().ToLower();
            ViewBag.page = page;
            ViewBag.rowid = rowid;
            model.ID = row_id;
            
            if (appmodule != null && appmodule.Count() > 0)
            {
                model.appmodule_select = new List<int>();
                model.appmodule_select = appmodule.ToList();
            }

            if (branchapp != null && branchapp.Count() > 0)
            {
                model.branch_select = new List<int>();
                model.branch_select = branchapp.ToList();
            }

            if (row_id > 0)
            {
                model.updateBy = curr_user.username;

                var result = new AppRoleBLL().Update(model);
                ViewBag.message_result = result.MessageResult;
                TempData["message_result"] = result.MessageResult;
                if (result.MessageResult != null && result.MessageResult.status == MessageStatus.success)
                    return RedirectToAction("Detail", new { rowid = UtilsBLL.Encrypt_QueryString(model.ID.ToString()), page = page, keyword = keyword, orderby = orderby, orderDESC = orderDESC });
            }
            else
            {
                model.createBy = curr_user.username;

                var result = new AppRoleBLL().Insert(model);
                model.ID = result.ID;
                ViewBag.message_result = result.MessageResult;
                TempData["message_result"] = result.MessageResult;
                if (result.MessageResult != null && result.MessageResult.status == MessageStatus.success)
                    return RedirectToAction("Detail", new { rowid = UtilsBLL.Encrypt_QueryString(model.ID.ToString()), page = page, keyword = keyword, orderby = orderby, orderDESC = orderDESC });
            }


            ViewBag.ActiveItem = this.ActiveItem();
            return View(model);
        }

        public PartialViewResult ConfirmDelete(int? page, string keyword, string orderby, string orderDESC, string rowid, string description)
        {
            orderby = (string.IsNullOrEmpty(orderby) ? "roleName" : orderby);

            ViewBag.rowid = rowid;
            ViewBag.description = description;
            bool desc = orderDESC == null ? false : orderDESC == "false" ? false : true;
            ViewBag.keyword = keyword;
            ViewBag.orderby = orderby;
            ViewBag.orderDESC = desc.ToString().ToLower();
            ViewBag.page = page;
            return PartialView("ConfirmDelete");
        }

        public ActionResult DeleteData(int? page, string keyword, string orderby, string orderDESC, string rowid)
        {
            int row_id = 0;
            try
            {
                var decode = UtilsBLL.Decrypt_QueryString(rowid);
                row_id = int.Parse(decode);
            }
            catch
            {
                row_id = 0;
            }

            var result = new AppRoleBLL().Delete(row_id);
            TempData["message_result"] = result.MessageResult;

            return DataBind(page, keyword, orderby, orderDESC);
        }

        public ActionResult PartialEmpty()
        {
            return new EmptyResult();
        }

    }
}
