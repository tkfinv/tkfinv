﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKFINV.Web.BusinessLogic;

namespace TKFINV.Web.Controllers
{
    public class InventoryController : Controller
    {
        //
        // GET: /Inventory/



        //private List<SelectListItem> FactoryItem(string roleid)
        //{
        //    List<SelectListItem> item_list = new List<SelectListItem>();
        //    var data = new AppRoleBLL().getRole();
        //    if (data != null && data.Count() > 0)
        //    {
        //        item_list.Add(new SelectListItem() { Value = "", Text = "Select All" });
        //        foreach (var item in data)
        //        {
        //            item_list.Add(new SelectListItem() { Value = item.ID.ToString(), Text = item.description, Selected = (roleid == item.ID.ToString() ? true : false) });
        //        }
        //    }

        //    return item_list;


        //}


        public ActionResult DataBind(string FactoryCode, string InventoryTypeCode, string ReferenceNumber, string DateRange)
        {
            DateTime dateTo = DateTime.Now.Date;
            DateTime dateFrom = DateTime.Now.Date.AddMonths(-1);
            if (!(DateRange == "" || DateRange ==null))
            {
                var strDateRange = DateRange.Split('-');
                dateFrom = UtilsBLL.ParseDate(strDateRange[0].Trim());
                dateTo = UtilsBLL.ParseDate(strDateRange[1].Trim());
            }
            ViewBag.DateRange = dateFrom.ToString("dd/MM/yyyy") + " - " + dateTo.ToString("dd/MM/yyyy");

            using (tkfinvEntities db = new tkfinvEntities())
            {

                var factoryList = (from f in db.factory where f.isActive == true orderby f.itemOrder select f).ToList();

                var inventoryTypeList = (from q in db.inventorytype orderby q.itemOrder select q).ToList();

                IEnumerable<inventory> inVentoryList = (from q in db.inventory select q).ToList();
               
                inVentoryList = (inVentoryList.Where(x => x.operateDate >= dateFrom && x.operateDate <= dateTo)).ToList();

                inVentoryList = (FactoryCode != "" && FactoryCode != null ? inVentoryList.Where(x => x.FactoryCode == FactoryCode) : inVentoryList).ToList();
                inVentoryList = (ReferenceNumber != "" && ReferenceNumber != null ? inVentoryList.Where(x => x.referenceNumber == ReferenceNumber) : inVentoryList).ToList();

                inVentoryList = (InventoryTypeCode != "" && InventoryTypeCode != null ? inVentoryList.Where(x => x.InventoryTypeCode == InventoryTypeCode) : inVentoryList).ToList();

                ViewBag.inVentoryList = inVentoryList.OrderByDescending(x => x.ID);
                
                
                ViewBag.factoryList = factoryList;

                List<SelectListItem> item_list = new List<SelectListItem>();
                foreach (var item in inventoryTypeList)
                {
                    item_list.Add(new SelectListItem() { Value = item.Code, Text = item.description, Selected = false });
                }
                ViewBag.itemListForDropdowbFactory = item_list;
                
            }
            return View();
        }



        public ActionResult Index(string FactoryCode, string InventoryTypeCode, string ReferenceNumber, string DateRange,string Success)
        {
            if (!(Success == "" || Success == null)) { ViewBag.Success = true; }

            ViewBag.DateRange = "";
            return DataBind(FactoryCode, InventoryTypeCode, ReferenceNumber, DateRange);

        }

        [HttpPost]
        public ActionResult Index(string FactoryCode, string InventoryTypeCode, string ReferenceNumber, string DateRange)
        {
            ViewBag.DateRange = DateRange;
            ViewBag.ReferenceNumber = ReferenceNumber;
            return DataBind(FactoryCode, InventoryTypeCode, ReferenceNumber, DateRange);
        }

        public ActionResult EditInventory(int ID)
        {
            using (tkfinvEntities db = new tkfinvEntities())
            {
                var dbObject = db.inventory.Where(x => x.ID == ID).FirstOrDefault();
                return PartialView(dbObject); 
            }
            
        }
    }
}
