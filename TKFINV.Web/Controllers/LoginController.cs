﻿using TKFINV.BusinessLogic;
using TKFINV.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKFINV.Web.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Index()
        {
            if (TempData["message_error"] != null)
            {
                ViewBag.message_error = TempData["message_error"];
                TempData["message_error"] = null;
            }

            return View();
        }


        [ValidateAntiForgeryToken]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(FormCollection form)
        {
            var user = form["txtuser"];
            var pass = form["txtpass"];

            if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(pass))
                ViewBag.message_error = "UserName && Password is required";
            else {

                var app_user = new LoginBLL().UserLogin(user, pass);
                if (!app_user.isActive)
                    ViewBag.message_error = app_user.loginNotifymessage;
                else
                {
                    Session["Appuser"] = app_user;
                    return RedirectToAction("Index", "Home");
                }

            }

            return View();
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            return RedirectToAction("Index", "Login", new { });
        }

    }
}
