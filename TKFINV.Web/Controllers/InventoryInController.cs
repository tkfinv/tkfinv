﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKFINV.Web.BusinessLogic;
using System.Text;

namespace TKFINV.Web.Controllers
{
    public class InventoryInController : Controller
    {
        //
        // GET: /InventoryIn/

        public ActionResult DataBind(string FactoryCode, string ReferenceNumber, string DateIn)
        {
            DateTime dateNow = DateTime.Now.Date;
           
            if (DateIn == "" || DateIn == null)
            {
                DateIn = dateNow.ToString("dd/MM/yyyy");
            }
            ViewBag.DateIn = DateIn.Trim(); 

            using (tkfinvEntities db = new tkfinvEntities())
            {
                 ////////////////////////////////////////////
                //get Factory
                var factoryList = (from f in db.factory where f.isActive == true orderby f.itemOrder select f).ToList();


                List<SelectListItem> item_list = new List<SelectListItem>();

                foreach (var item in factoryList)
                {
                    item_list.Add(new SelectListItem() { Value = item.Code, Text = item.description, Selected = false });
                }

                ViewBag.factoryList = item_list;
                ////////////////////////////////////////////////////////////////
                //get product
                var productList = (from f in db.productmodel  orderby f.Code select f).ToList();
                List<SelectListItem> product_list = new List<SelectListItem>();
                
                foreach (var item in productList)
                {
                    product_list.Add(new SelectListItem() { Value = item.Code, Text = item.Code.ToString() +" " +item.productName.ToString(), Selected = false });
                }

                ViewBag.productList = product_list;

                //////////////////////////////////////////////////////////////////////

                //getSize
                var sizeList = (from f in db.productsize orderby f.Code select f).ToList();
                List<SelectListItem> size_list = new List<SelectListItem>();

                foreach (var item in sizeList)
                {
                    size_list.Add(new SelectListItem() { Value = item.Code, Text = item.Code , Selected = false });
                }

                ViewBag.sizetList = size_list;

                ////////////////////////////////////////////////////////////
                //getLocation
                var locationList = (from f in db.location orderby f.Code select f).ToList();
                List<SelectListItem> location_list = new List<SelectListItem>();

                foreach (var item in locationList)
                {
                    location_list.Add(new SelectListItem() { Value = item.Code, Text = item.description, Selected = false });
                }

                ViewBag.locationList = location_list;

                ////////////////////////////////////////////////////////////////

                //get package

                var packageList = (from f in db.packageunit orderby f.Code select f).ToList();
                List<SelectListItem> package_list = new List<SelectListItem>();

                foreach (var item in packageList)
                {
                    package_list.Add(new SelectListItem() { Value = item.Code, Text = item.description, Selected = false });
                }

                ViewBag.packageList = package_list;


            
            
            }
            return View();
        }


        public ActionResult Index(string FactoryCode, string ReferenceNumber, string DateIn)
        {
           // ViewBag.DateRange = "";
            return DataBind(FactoryCode, ReferenceNumber, DateIn);
              
              
             // DataBind(FactoryCode, ReferenceNumber, DateIn);

        }


        [HttpPost]
        public ActionResult Index(string FactoryCode, string ReferenceNumber, string DateIn, string[] ProductCode, string[] ProductSizeCode, string[] LocationCode, string[] PackageCode, string[] Quantity)
        {
            // ViewBag.DateRange = "";
            try
            {
                //Insert record into ddatabase

                if (ReferenceNumber=="")
                {
                    ViewBag.Success = false;
                    ViewBag.ErrorMsg = "กรุณาใส่หมายเลข Po/Inv ก่อนทำการบันทึก";
                    FactoryCode = "";
                    ReferenceNumber = "";
                    DateIn = "";
                    return DataBind(FactoryCode, ReferenceNumber, DateIn);
                }

                //using (tkfinvEntities db = new tkfinvEntities())
                //{

                //    var CheckPO = (from q in db.inventory where q.referenceNumber == ReferenceNumber select q).ToList();
                //    //  inVentorycurrentlyList = (ProductCode[i] != "" ? inVentorycurrentlyList.Where(x => x.ProductModelCode == ProductCode[i]) : inVentorycurrentlyList).ToList();

                //    if (CheckPO.Count() != 0)
                //    {

                //        ViewBag.Success = false;
                //        ViewBag.ErrorMsg = "ไม่สามารถบันทึกได้เนื่องจากหมายเลข Po/Inv :" + ReferenceNumber + " นี้มีอยู่ในระบบแล้ว";
                //        FactoryCode = "";
                //        ReferenceNumber = "";
                //        DateIn = "";
                //        return DataBind(FactoryCode, ReferenceNumber, DateIn);
                //    }

                
                
                //}
              



                int countRow = ProductCode.Count();

                for (int i = 0; i < countRow; i++)
                {

                    if (ProductCode[i] != "" && ProductSizeCode[i] != "" && LocationCode[i] != "" && PackageCode[i] != "" && Quantity[i] != "")
                    {
                        using (tkfinvEntities db = new tkfinvEntities())


                        {

                          //  var dbObj = (from q in db.inventorycurrently select q).ToList();// ดึงข้อมูล existingQuantity จาก inventorycurrently
                            IEnumerable<inventorycurrently> inVentorycurrentlyList = (from q in db.inventorycurrently select q).ToList();
                            inVentorycurrentlyList = (ProductCode[i] != "" ? inVentorycurrentlyList.Where(x => x.ProductModelCode == ProductCode[i]) : inVentorycurrentlyList).ToList();
                            inVentorycurrentlyList = (ProductSizeCode[i] != "" ? inVentorycurrentlyList.Where(x => x.ProductSizeCode == ProductSizeCode[i]) : inVentorycurrentlyList).ToList();
                            inVentorycurrentlyList = (LocationCode[i] != "" ? inVentorycurrentlyList.Where(x => x.LocationCode == LocationCode[i]) : inVentorycurrentlyList).ToList();
                            inVentorycurrentlyList = (PackageCode[i] != "" ? inVentorycurrentlyList.Where(x => x.PackageUnitCode == PackageCode[i]) : inVentorycurrentlyList).ToList();
                           
                            // where q.ProductModelCode == ProductCode[i] && q.ProductSizeCode == ProductSizeCode[i] && q.LocationCode == LocationCode[i] && q.PackageUnitCode == PackageCode[i] 
                            
                            var data = new inventory();
                            if (inVentorycurrentlyList.Count() == 0)
                            {
                                data.existingQuantity = 0;
                            }
                            else
                            {
                                data.existingQuantity = 0;
                                Int32 Qtyexit = 0;
                                foreach (TKFINV.Web.inventorycurrently item in inVentorycurrentlyList)

                                {
                                    Int32.TryParse(item.remaindQuantity, out Qtyexit);
                                    data.existingQuantity = data.existingQuantity + Qtyexit;

                                   
                                }

                            }

                            var user = (TKFINV.Web.Models.LoginUserModel)Session["Appuser"];

                            data.FactoryCode = FactoryCode;
                            data.referenceNumber = ReferenceNumber;
                            data.InventoryTypeCode = "IN";
                            data.ProductModelCode = ProductCode[i];
                            data.ProductSizeCode = ProductSizeCode[i];
                            data.PackageUnitCode = PackageCode[i];
                            data.LocationCode = LocationCode[i];
                            data.quantity = Quantity[i];
                            data.createDate = DateTime.Now;
                            data.operateDate = UtilsBLL.ParseDate(DateIn);
                            data.createBy = user.username;
                            db.Entry(data).State = System.Data.EntityState.Added;
                            db.SaveChanges();

                           
                        }

                    }
                
                
                
                }


                    ViewBag.Success = true;
                    return RedirectToAction("Index", "Inventory", new { Success = true });
                   
            }
            catch(Exception exc)
            {
                ViewBag.Success = false;
                ViewBag.ErrorMsg = exc.Message;
            }


            FactoryCode = "";
            ReferenceNumber = "";
            DateIn = "";


            return DataBind(FactoryCode, ReferenceNumber, DateIn);


            // DataBind(FactoryCode, ReferenceNumber, DateIn);

        }

    }
}
