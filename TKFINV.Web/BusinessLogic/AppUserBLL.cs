﻿using TKFINV.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TKFINV.BusinessLogic;

namespace TKFINV.Web.BusinessLogic
{
    public class AppUserBLL
    {
        public IEnumerable<applicationuser> getList(string keyword, string keyrole, string orderby, bool orderDESC)
        {
            IEnumerable<applicationuser> objectResult = null;
            using (tkfinvEntities db = new tkfinvEntities())
            {
                string param = orderby == null ? "username" : orderby == "" ? "username" : orderby;
                keyword = keyword == null ? "" : keyword;
                var pi = typeof(applicationuser).GetProperty(param);
                objectResult = (from q in db.applicationuser select q).ToList();

                objectResult = keyword == "" ? objectResult : objectResult.Where(x => x.username.Contains(keyword) || x.firstName.Contains(keyword));
                objectResult = keyrole == null ? objectResult : objectResult.Where(x => x.ApplicationuserroleID.ToString().Contains(keyrole));

                objectResult = orderDESC ?
                    objectResult.OrderByDescending(x => pi.GetValue(x, null)).ToList() :
                    objectResult.OrderBy(x => pi.GetValue(x, null)).ToList();


            }

            //replace value
            using (tkfinvEntities db = new tkfinvEntities())
            {
                foreach (var item in objectResult)
                {
                    item.applicationuserrole = (from w in db.applicationuserrole where w.ID == item.ApplicationuserroleID select w).FirstOrDefault();
                }
            }

            return objectResult;
        }

        public IEnumerable<applicationmodule> getAllModule()
        {
            IEnumerable<applicationmodule> objectResult = null;
            using (tkfinvEntities db = new tkfinvEntities())
            {
                objectResult = (from q in db.applicationmodule where q.isActive == true orderby q.itemOrder select q).ToList();
            }
            return objectResult;
        }

        public applicationuserModel getByUserName(string username)
        {
            applicationuserModel modelObj = new applicationuserModel();
            using (tkfinvEntities db = new tkfinvEntities())
            {
                var dbObj = (from q in db.applicationuser where q.username.ToLower() == username.ToLower() select q).FirstOrDefault();

                if (dbObj != null)
                {
                    modelObj.ID = dbObj.ID;
                    modelObj.username = dbObj.username;
                    modelObj.password = dbObj.password;
                    modelObj.firstName = dbObj.firstName;
                    modelObj.lastName = dbObj.lastName;
                    modelObj.email = dbObj.email;
                    modelObj.isActive = dbObj.isActive;
                    modelObj.ApplicationUserRoleID = dbObj.ApplicationuserroleID;
                    modelObj.applicationuserrole = (from q in db.applicationuserrole where q.ID == dbObj.ApplicationuserroleID select q).FirstOrDefault();
                    modelObj.avatarFile = dbObj.avatarFile;
                    modelObj.city = dbObj.city;
                    modelObj.country = dbObj.country;
                    modelObj.createBy = dbObj.createBy;
                    modelObj.createDate = dbObj.createDate;
                    modelObj.fax = dbObj.fax;
                    modelObj.lastLogin = dbObj.lastLogin;
                    modelObj.phone1 = dbObj.phone1;
                    modelObj.phone2 = dbObj.phone2;
                    modelObj.province = dbObj.province;
                    modelObj.updateBy = dbObj.updateBy;
                    modelObj.updateDate = dbObj.updateDate;
                    modelObj.zipcode = dbObj.zipcode;
                }
            }
            return modelObj;
        }

        public applicationuserModel getModelObjectByID(int ID)
        {
            applicationuserModel modelObj = new applicationuserModel();
            using (tkfinvEntities db = new tkfinvEntities())
            {
                var dbObj = (from q in db.applicationuser where q.ID == ID select q).FirstOrDefault();

                if (dbObj != null)
                {
                    modelObj.ID = dbObj.ID;
                    modelObj.username = dbObj.username;
                    modelObj.password = dbObj.password;
                    modelObj.firstName = dbObj.firstName;
                    modelObj.lastName = dbObj.lastName;
                    modelObj.email = dbObj.email;
                    modelObj.isActive = dbObj.isActive;
                    modelObj.ApplicationUserRoleID = dbObj.ApplicationuserroleID;
                    modelObj.applicationuserrole = (from q in db.applicationuserrole where q.ID == dbObj.ApplicationuserroleID select q).FirstOrDefault();
                    modelObj.avatarFile = dbObj.avatarFile;
                    modelObj.city = dbObj.city;
                    modelObj.country = dbObj.country;
                    modelObj.createBy = dbObj.createBy;
                    modelObj.createDate = dbObj.createDate;
                    modelObj.fax = dbObj.fax;
                    modelObj.lastLogin = dbObj.lastLogin;
                    modelObj.phone1 = dbObj.phone1;
                    modelObj.phone2 = dbObj.phone2;
                    modelObj.province = dbObj.province;
                    modelObj.updateBy = dbObj.updateBy;
                    modelObj.updateDate = dbObj.updateDate;
                    modelObj.zipcode = dbObj.zipcode;
                }
            }
            return modelObj;
        }

        public applicationuserModel Update(applicationuserModel model)
        {
            try
            {
                using (tkfinvEntities db = new tkfinvEntities())
                {
                    var data = (from q in db.applicationuser where q.ID == model.ID select q).FirstOrDefault();
                    if (data != null)
                    {
                        if (!data.email.ToLower().Equals(model.email.ToLower()))
                        {
                            var check_email = (from q in db.applicationuser where q.email == model.email select q).FirstOrDefault();
                            if (check_email != null)
                            {
                                model.MessageResult = new MessageResult() { status = MessageStatus.warning, message = "Email นี้มีการใช้งานแล้ว กรุณาตรวจสอบใหม่ อีกครั้ง" };
                                return model;
                            }
                        }

                        data.ID = model.ID;
                        data.address = model.address;
                        if (model.ApplicationUserRoleID != 0)
                            data.ApplicationuserroleID = model.ApplicationUserRoleID;

                        data.avatarFile = model.avatarFile;
                        data.city = model.city;
                        data.country = model.country;
                        //data.createBy = model.createBy;
                        //data.createDate = model.createDate;
                        data.email = model.email;
                        data.fax = model.fax;
                        data.firstName = model.firstName;
                        if (data.username.Equals("pcsadmin"))
                            data.isActive = true;
                        else
                            data.isActive = model.isActive;
                        //data.lastLogin = module.lastLogin;
                        data.lastName = model.lastName;


                        if (!string.IsNullOrEmpty(model.password) && !string.IsNullOrEmpty(model.confirmPassword))
                        {
                            if (model.password.Equals(model.confirmPassword))
                            {
                                var newpassword = model.username + model.password;
                                data.password = new LoginBLL().GetMd5Hash(newpassword);
                            }
                        }

                        data.phone1 = model.phone1;
                        data.phone2 = model.phone2;
                        data.province = model.province;
                        data.updateBy = model.updateBy;
                        data.updateDate = DateTime.Now;
                        //data.username = model.username;
                        data.zipcode = model.zipcode;

                        db.Entry(data).State = System.Data.EntityState.Modified;
                        db.SaveChanges();
                        model.ID = data.ID;
                        model.MessageResult = new MessageResult() { status = MessageStatus.success, message = new MessageText().update };

                    }
                }
            }
            catch (Exception ex)
            {
                model.MessageResult = new MessageResult() { status = MessageStatus.error, message = (ex.InnerException == null ? ex.Message : ex.InnerException.Message) };
            }

            return model;
        }

        public applicationuserModel Insert(applicationuserModel model)
        {
            try
            {
                using (tkfinvEntities db = new tkfinvEntities())
                {
                    var data = new applicationuser();
                    var check_username = (from q in db.applicationuser where q.username == model.username select q).FirstOrDefault();
                    if (check_username != null)
                    {
                        model.MessageResult = new MessageResult() { status = MessageStatus.warning, message = "User นี้มีการใช้งานแล้ว กรุณาตรวจสอบใหม่ อีกครั้ง" };
                        return model;
                    }

                    var check_email = (from q in db.applicationuser where q.email == model.email select q).FirstOrDefault();
                    if (check_email != null)
                    {
                        model.MessageResult = new MessageResult() { status = MessageStatus.warning, message = "Email นี้มีการใช้งานแล้ว กรุณาตรวจสอบใหม่ อีกครั้ง" };
                        return model;
                    }

                    data.ID = model.ID;
                    data.username = model.username;
                    data.address = model.address;
                    data.ApplicationuserroleID = model.ApplicationUserRoleID;
                    data.avatarFile = model.avatarFile;
                    data.city = model.city;
                    data.country = model.country;
                    data.createBy = model.createBy;
                    data.createDate = DateTime.Now;
                    data.email = model.email;
                    data.fax = model.fax;
                    data.firstName = model.firstName;
                    data.isActive = model.isActive;
                    //data.lastLogin = module.lastLogin;
                    data.lastName = model.lastName;


                    if (model.password.Equals(model.confirmPassword))
                    {
                        var newpassword = model.username + model.password;
                        data.password = new LoginBLL().GetMd5Hash(newpassword);
                    }


                    data.phone1 = model.phone1;
                    data.phone2 = model.phone2;
                    data.province = model.province;
                    data.updateBy = model.updateBy;
                    //data.updateDate = DateTime.Now;
                    //
                    data.zipcode = model.zipcode;

                    db.Entry(data).State = System.Data.EntityState.Added;
                    db.SaveChanges();
                    model.ID = data.ID;
                    model.MessageResult = new MessageResult() { status = MessageStatus.success, message = new MessageText().insert };

                }
            }
            catch (Exception ex)
            {
                model.MessageResult = new MessageResult() { status = MessageStatus.error, message = (ex.InnerException != null ? ex.InnerException.Message : ex.Message) };
            }

            return model;
        }

        public applicationuserModel Delete(int Id)
        {
            var model = new applicationuserModel();

            try
            {
                using (tkfinvEntities db = new tkfinvEntities())
                {

                    var data = (from q in db.applicationuser where q.ID == Id select q).FirstOrDefault();
                    if (data != null)
                    {
                        db.applicationuser.Remove(data);
                        db.SaveChanges();
                        model.MessageResult = new MessageResult() { status = MessageStatus.success, message = new MessageText().delete };
                    }
                }
            }
            catch (Exception ex)
            {
                model.MessageResult = new MessageResult() { status = MessageStatus.error, message = (ex.InnerException != null ? ex.InnerException.Message : ex.Message) };
            }

            return model;
        }
    }
}