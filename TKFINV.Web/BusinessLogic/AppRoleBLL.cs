﻿using TKFINV.Web.Models;
using TKFINV.Web.BusinessLogic;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace TKFINV.Web.BusinessLogic
{
    public class AppRoleBLL
    {
        public IEnumerable<applicationuserrole> getRole()
        {
            IEnumerable<applicationuserrole> roles = null;

            using (tkfinvEntities db = new tkfinvEntities())
            {
                roles = (from q in db.applicationuserrole orderby q.ID ascending select q).ToList();
            }


            return roles;
        }

        public List<applicationuserroleModel> getList(string keyword, string orderby, bool orderDESC)
        {
            IEnumerable<applicationuserrole> objectResult = null;
            List<applicationuserroleModel> objectResultmodel = new List<applicationuserroleModel>();
            using (tkfinvEntities db = new tkfinvEntities())
            {
                var pi = typeof(applicationuserrole).GetProperty(orderby);
                objectResult = (from q in db.applicationuserrole select q).ToList();

                if (!string.IsNullOrEmpty(keyword))
                {
                    var q1 = (from q in objectResult where q.roleName != null && q.description != null select q);
                    var result = q1.Where(x => x.roleName.ToLower().Contains(keyword.ToLower()) || x.description.ToLower().Contains(keyword));
                    objectResult = result;
                }

                objectResult = orderDESC ?
                    objectResult.OrderByDescending(x => pi.GetValue(x, null)).ToList() :
                    objectResult.OrderBy(x => pi.GetValue(x, null)).ToList();

                foreach (var item in objectResult)
                {
                    applicationuserroleModel a = new applicationuserroleModel();
                    a.ID = item.ID;
                    a.UserRoleCount = (from q in db.applicationuser where q.ApplicationuserroleID == item.ID select q).Count();
                    a.roleName = item.roleName;
                    a.description = item.description;
                    a.isActive = item.isActive;
                    a.createDate = item.createDate;
                    a.createBy = item.createBy;
                    a.updateBy = item.updateBy;
                    a.updateDate = item.updateDate;

                    objectResultmodel.Add(a);
                }
            }

            return objectResultmodel;
        }

        public List<applicationmodule> getAllModule()
        {
            List<applicationmodule> objectResult = null;
            using (tkfinvEntities db = new tkfinvEntities())
            {
                objectResult = (from q in db.applicationmodule where q.isActive == true orderby q.itemOrder select q).ToList();
            }
            return objectResult;
        }

        public List<applicationmodulegroup> getAllModuleGroup()
        {
            List<applicationmodulegroup> objectResult = new List<applicationmodulegroup>();
            using (tkfinvEntities db = new tkfinvEntities())
            {
                objectResult = (from q in db.applicationmodulegroup where q.isActive == true orderby q.itemOrder select q).ToList();
            }
            return objectResult;
        }

        public applicationuserroleModel getByID(int ID)
        {
            applicationuserroleModel modelObj = new applicationuserroleModel();
            using (tkfinvEntities db = new tkfinvEntities())
            {
                var dbObj = (from q in db.applicationuserrole where q.ID == ID select q).FirstOrDefault();
                if (dbObj != null)
                {
                    modelObj.ID = dbObj.ID;
                    modelObj.roleName = dbObj.roleName;
                    modelObj.description = dbObj.description;
                    modelObj.isActive = dbObj.isActive;
                    modelObj.createBy = dbObj.createBy;
                    modelObj.createDate = dbObj.createDate;
                    modelObj.updateBy = dbObj.updateBy;
                    modelObj.updateDate = dbObj.updateDate;

                    modelObj.moduleList = new List<applicationmodule>();
                    //modelObj.branchList = new List<branch>();
                    //modelObj.companyList = new List<company>();

                    if (modelObj.ID.Equals(1))
                    {
                        var modules = (from q in db.applicationmodule select q).ToList();
                        //var branchs = (from q in db.branches select q).ToList();

                        modelObj.moduleList = modules;
                        //modelObj.branchList = branchs;

                    }
                    else
                    {
                        var module_id = (from q in db.appuserrole_module where q.ApplicationUserRoleID == modelObj.ID select q.ApplicationModuleID).ToList();
                        var modules = (from q in db.applicationmodule where module_id.Contains(q.ID) select q).ToList();
                        modelObj.moduleList = modules;

                        //var branch_id = (from q in db.appuserrole_branch where q.ApplicationUserRoleID == modelObj.ID select q.BranchID).ToList();
                        //var branchs = (from q in db.branches where branch_id.Contains(q.ID) select q).ToList();
                        //modelObj.branchList = branchs;
                    }

                    //var companys = (from q in db.companies select q).ToList();
                    //modelObj.companyList = companys;

                }
            }
            return modelObj;
        }

        public applicationuserroleModel Update(applicationuserroleModel model)
        {
            try
            {
                using (tkfinvEntities db = new tkfinvEntities())
                {
                    var data = (from q in db.applicationuserrole where q.ID == model.ID select q).FirstOrDefault();
                    if (data != null)
                    {
                        data.ID = model.ID;
                        data.roleName = model.roleName;
                        data.description = model.description;
                        data.isActive = model.isActive;

                        //data.createBy = model.createBy;
                        //data.createDate = model.createDate;
                        data.updateBy = model.updateBy;
                        data.updateDate = DateTime.Now;

                        db.Entry(data).State = System.Data.EntityState.Modified;
                        db.SaveChanges();
                        model.ID = data.ID;

                        model.appmodule_select = model.appmodule_select == null ? new List<int>() : model.appmodule_select;
                        model.branch_select = model.branch_select == null ? new List<int>() : model.branch_select;

                        //remove all role_module
                        var role_module = (from q in db.appuserrole_module where q.ApplicationUserRoleID == data.ID select q).ToList();
                        foreach (var item in role_module)
                            db.appuserrole_module.Remove(item);

                        //remove all role_branch
                        //var role_branch = (from q in db.appuserrole_branch where q.ApplicationUserRoleID == data.ID select q).ToList();
                        //foreach (var item in role_branch)
                        //    db.appuserrole_branch.Remove(item);

                        //add all select module for this role
                        foreach (var item in model.appmodule_select)
                        {
                            appuserrole_module a = new appuserrole_module();
                            a.ApplicationModuleID = item;
                            a.ApplicationUserRoleID = data.ID;
                            a.createBy = model.updateBy;
                            a.createDate = DateTime.Now;
                            db.appuserrole_module.Add(a);
                        }
                        db.SaveChanges();

                        //add all select branch for this role
                        //foreach (var item in model.branch_select)
                        //{
                        //    appuserrole_branch a = new appuserrole_branch();
                        //    a.BranchID = item;
                        //    a.ApplicationUserRoleID = data.ID;
                        //    a.createBy = model.updateBy;
                        //    a.createDate = DateTime.Now;
                        //    db.appuserrole_branch.Add(a);
                        //}
                        //db.SaveChanges();

                        model.MessageResult = new MessageResult() { status = MessageStatus.success, message = new MessageText().update };

                    }
                }
            }
            catch (Exception ex)
            {
                model.MessageResult = new MessageResult() { status = MessageStatus.error, message = (ex.InnerException == null ? ex.Message : ex.InnerException.Message) };
            }

            return model;
        }

        public applicationuserroleModel Insert(applicationuserroleModel model)
        {
            try
            {
                using (tkfinvEntities db = new tkfinvEntities())
                {
                    var data = new applicationuserrole();
                    data.ID = model.ID;
                    data.roleName = model.roleName;
                    data.description = model.description;
                    data.isActive = model.isActive;
                    data.createBy = model.createBy;
                    data.createDate = DateTime.Now;
                    //data.updateBy = model.updateBy;
                    //data.updateDate = DateTime.Now;

                    db.Entry(data).State = System.Data.EntityState.Added;
                    db.SaveChanges();
                    model.ID = data.ID;

                    model.appmodule_select = model.appmodule_select == null ? new List<int>() : model.appmodule_select;
                    model.branch_select = model.branch_select == null ? new List<int>() : model.branch_select;

                    //remove all role_module
                    var role_module = (from q in db.appuserrole_module where q.ApplicationUserRoleID == data.ID select q).ToList();
                    foreach (var item in role_module)
                        db.appuserrole_module.Remove(item);

                    //remove all role_branch
                    //var role_branch = (from q in db.appuserrole_branch where q.ApplicationUserRoleID == data.ID select q).ToList();
                    //foreach (var item in role_branch)
                    //    db.appuserrole_branch.Remove(item);

                    //add all select module for this role
                    foreach (var item in model.appmodule_select)
                    {
                        appuserrole_module a = new appuserrole_module();
                        a.ApplicationModuleID = item;
                        a.ApplicationUserRoleID = data.ID;
                        a.createBy = model.updateBy;
                        a.createDate = DateTime.Now;
                        db.appuserrole_module.Add(a);
                    }
                    db.SaveChanges();

                    //add all select branch for this role
                    //foreach (var item in model.branch_select)
                    //{
                    //    appuserrole_branch a = new appuserrole_branch();
                    //    a.BranchID = item;
                    //    a.ApplicationUserRoleID = data.ID;
                    //    a.createBy = model.updateBy;
                    //    a.createDate = DateTime.Now;
                    //    db.appuserrole_branch.Add(a);
                    //}
                    //db.SaveChanges();

                    model.MessageResult = new MessageResult() { status = MessageStatus.success, message = new MessageText().insert };

                }
            }
            catch (Exception ex)
            {
                model.MessageResult = new MessageResult() { status = MessageStatus.error, message = (ex.InnerException != null ? ex.InnerException.Message : ex.Message) };
            }

            return model;
        }

        public applicationuserroleModel Delete(int Id)
        {
            var model = new applicationuserroleModel();

            try
            {
                using (tkfinvEntities db = new tkfinvEntities())
                {

                    var data = (from q in db.applicationuserrole where q.ID == Id select q).FirstOrDefault();
                    if (data != null)
                    {
                        db.applicationuserrole.Remove(data);
                        db.SaveChanges();
                        model.MessageResult = new MessageResult() { status = MessageStatus.success, message = new MessageText().delete };
                    }
                }
            }
            catch (Exception ex)
            {
                model.MessageResult = new MessageResult() { status = MessageStatus.error, message = (ex.InnerException != null ? ex.InnerException.Message : ex.Message) };
            }

            return model;
        }
    }
}