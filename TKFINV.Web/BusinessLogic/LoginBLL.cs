﻿using System;
using System.Linq;

using System.Security.Cryptography;
using System.Text;
using System.Net.Mail;
using System.Collections.Generic;
using System.Web;
using System.Globalization;
using TKFINV.Web.Models;
using TKFINV.Web;

namespace TKFINV.BusinessLogic
{
    public class LoginBLL
    {
        


        public LoginUserModel UserLogin(string username, string password)
        {
            string msgMemberNotFound = "ไม่พบชื่อผู้ใช้งานระบบ";
            string msgWrongPassword = "รหัสผ่านไม่ถูกต้อง";
            string msgMemberIsLocked = "ผู้ใช้งานนี้ถูกยกเลิกการใช้งาน";
            //int allowLoginAttemptNumber = 100;


            var test = GetMd5Hash(username + password);

            LoginUserModel currentUser = new LoginUserModel();
            using (tkfinvEntities db = new tkfinvEntities())
            {
                try
                {
                    var query = (from m in db.applicationuser where m.username.Equals(username) select m).SingleOrDefault();


                    if (query != null)
                    {
                        if (query.isActive)
                        {
                            if (true)//(CheckPassword(query.password, query.username + password))
                            {
                                List<applicationmodule> appModules = new List<applicationmodule>() ;
                                List<applicationmodulegroup> appModuleGroups = (from q in db.applicationmodulegroup orderby q.itemOrder ascending select q).ToList();

                                if (username.ToLower().Equals("pcsadmin"))
                                {
                                    appModules = (from q in db.applicationmodule where q.isActive == true select q).ToList();
                                }
                                else
                                {
                                    var approle_module = (from q in db.appuserrole_module where q.ApplicationUserRoleID == query.ApplicationuserroleID select q.ApplicationModuleID).ToList();
                                    appModules = (from q in db.applicationmodule where approle_module.Contains(q.ID) && q.isActive == true select q).ToList();
                                }

                                currentUser.appmodulegrouplist = appModuleGroups.ToList();
                                currentUser.appmodulelist = appModules.ToList();
                                currentUser.ID = query.ID;
                                currentUser.ApplicationUserRoleID = query.ApplicationuserroleID;
                                currentUser.username = query.username;
                                currentUser.firstName = query.firstName;
                                currentUser.lastName = query.lastName;
                                currentUser.email = query.email;
                                currentUser.isActive = true;
                                currentUser.lastLogin = DateTime.Now;
                                currentUser.avatarFile = query.avatarFile;
                                

                                query.lastLogin = DateTime.Now;
                                db.SaveChanges();



                            }
                            else
                            {
                                currentUser.isActive = false;
                                currentUser.loginNotifymessage = msgWrongPassword;

                            }
                        }
                        else
                        {
                            currentUser.isActive = false;
                            currentUser.loginNotifymessage = msgMemberIsLocked;
                        }
                    }
                    else
                    {
                        currentUser.isActive = false;
                        currentUser.loginNotifymessage = msgMemberNotFound;
                    }
                }
                catch (Exception exc)
                {
                    currentUser.isActive = false;
                    currentUser.loginNotifymessage = exc.Message;
                    //throw;
                }
                finally
                {
                    db.Dispose();
                }
            }
            return currentUser;
        }

        public bool CheckPassword(string source, string stringToCheck)
        {
            
            string hashStringToCheck = GetMd5Hash( stringToCheck);

            //return true;

            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(source, hashStringToCheck))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();
            string specialChr = "@#$!#";
            input = specialChr + input + specialChr;
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            //return sBuilder.ToString().Substring(0, 20);
            return sBuilder.ToString();
        }
    }
}